# Shell Tester

<h4>How to Use

Works only with windows so far, and Windows subsystem for linux(WSL). Make sure python 3.7+
is installed. I installed it im my WSL (Ubuntu) and the interpreter is definede at the
top of the tester. So if you are not using WSL you might have to change that
to get it to work. I have no idea how this script will work since it was developed just
for my machine, and my machine uses the complicated dancing of Windows and WSL together.
Using purely Windows doesn't really make anything better. Oh the days I long for pure linux...<br>
Open cmd where your compiled shell is and type 

> python ShellTestHarness.py

to run it. Make sure the compiled shell is called a.out. This will test cd, pwd, ls, and echo
(not the $? argument). I don't know how to test the exit command yet, and it doesn't tell you what
tests you got right, only what you got wrong. So that needs to be fixed. But it's a very good step
in the right direction for me learning how to make automatic test scripts in python.

If you want to make it better, fork and make a merge request :)
