#!/usr/bin/python3.7

import os
import subprocess
from subprocess import PIPE

# global variables :(

args = ['C:\\Windows\\SysNative\\bash.exe', '-c', './a.out']
tests_passed = 0
tests_failed = 0
failed_methods = []


def test(method):
    """
    This method calls every function tagged @test and counts the passes and fails

    :param method: A method to test
    :return: nothing!
    """
    global tests_passed
    global tests_failed

    try:
        method()
        tests_passed += 1

    except AssertionError as e:
        tests_failed += 1
        failed_methods.append(method.__name__)
        try:
            failed_methods.append(e.args[0])
        except IndexError:
            print("\nWARNING: " + method.__name__ + "() includes an assertion with no fail message\n")


def get_dir(path):
    """
    Turns a Windows or *nix path into an array with each directory being an element in that array.
    :param path: The path to parse into an array.
    :return: An array containing each directory of path.
    """

    dir = []
    j = -1
    start_counting = False

    # gets each directory from path and puts it in dir[]
    for i in range(0, len(path)):

        # end of line so stop counting
        if path[i] is "\n":
            break

        # gets a directory, splits on the slashes
        if start_counting:
            if path[i] in ['/', '\\']:
                dir.append('')
                j += 1
            else:
                dir[j] += path[i]

        # start counting
        if path[i] is "$":
            start_counting = True
        elif path[i] is "C" and path[i+1] is ":":
            start_counting = True
            dir.append('C')
            j += 1

    return dir


def compare_dirs(dir1, dir2):
    """
    Compares two pathways to see if they are the same. Can compare them even if one is from
    Windows and the other is from Linux.

    :param dir1: A path
    :param dir2: A path
    :return: True if they both are the same.
    """

    i = 0
    j = 0

    # ignores mnt
    if dir1[i] == 'mnt':
        i += 1
    if dir2[j] == 'mnt':
        j += 1

    # are they the same length?
    if len(dir1) - i != len(dir2) - j:
        return False

    # checks if these two paths are the same
    for i in range(i, len(dir1)):
        if dir1[i] != dir2[j]:

            # treats 'c' and "C:" as the same
            if (dir1[i] is 'C:' and dir2[j] is not 'c') and (dir1[i] is 'c' and dir2[j] is not 'C:'):
                return False # dont do this at home kids
        j += 1

    return True


def get_echo_return(output):
    """
    Takes in the output from a shell.communicate("echo ...) expression and parses the output to find exactly what was
    returned by echo

    :param output:
    :return:
    """
    echo_return = ''
    counting = False
    done = False
    i = 0
    while i < len(output) and not done:
        if counting:
            if output[i] is '/':
                done = True
            else:
                echo_return += output[i]

        if output[i] is "$":
            counting = True
        i += 1
    return echo_return


@test
def test_cd():
    # test relative paths
    shell = subprocess.Popen(args, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE, encoding='utf8')
    assert shell.communicate("cd cmake-build-debug")[1] in ['', None], "Cannot use relative paths"

    # test absolute paths
    shell = subprocess.Popen(args, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE, encoding='utf8')
    assert shell.communicate("cd /mnt/c")[1] in ['', None], "Cannot change to an absolute path."

    # test changing to parent directory
    shell = subprocess.Popen(args, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE, encoding='utf8')
    assert shell.communicate("cd ..")[1] in ['', None], "Cannot recede to parent directory"

    # test traversal to non-existent directory
    shell = subprocess.Popen(args, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE, encoding='utf8')
    assert shell.communicate("cd i-dont-exist-673898764478")[1] not in ['', None], "No error on traversal to non-existent directory."


@test
def test_ls():
    # test printing ls
    shell = subprocess.Popen(args, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE, encoding='utf8')
    assert shell.communicate("ls")[0] not in ['', None], "Doesn't list directory contents."

    # test ls with arguments
    shell = subprocess.Popen(args, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE, encoding='utf8')
    assert shell.communicate("ls -l")[0] not in ['', None], "Doesn't accept parameters"


@test
def test_pwd():
    # test if pwd is correct
    shell = subprocess.Popen(args, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE, encoding='utf8')
    output = shell.communicate("pwd")
    assert output[1] in ['', None], "pwd not implemented"
    assert compare_dirs(get_dir(output[0]), get_dir(os.getcwd())), "Incorrect working directory"

    # Test pdw with parameters
    shell = subprocess.Popen(args, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE, encoding='utf8')
    assert shell.communicate("pwd crap")[1] in ['', None], "pwd doesn't take parameters and therefore shouldn't act" \
                                                           "different if parameters are present"

@test
def test_echo():
    # test if echo exists and works with normal input
    shell = subprocess.Popen(args, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE, encoding='utf8')
    output = shell.communicate("echo hello")
    assert output[1] in ['', None], "Echo might not be implemented"
    assert get_echo_return(output[0]) == "hello\n", "Doesn't return correct information"

    # tests if echo doesn't print new lines when told to
    shell = subprocess.Popen(args, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE, encoding='utf8')
    output = shell.communicate("echo -n hello")
    assert get_echo_return(output[0]) == "hello", "-n parameter doesn't work as expected"

    # test if special echo command works


#todo: how to test exit???

# def test_exit():
#     # test exit command
#     shell = subprocess.Popen(args, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE, encoding='utf8')
#     shell.stdin.write("exit\n")
#
#     assert shell.poll() is not None, "Shell doesn't quit running"


# tell user how many methods failed
if (tests_failed > 0):
    print('\n')
    print('-'*80)
    print("FAILURE!!!    ", end='')
    print(str(tests_failed) + " failed" +
          " test." if tests_failed == 1 else " tests.", end='\n\n')
else:
    print("Success! All tests passed.")

# tell user which methods failed
for i in range(0, len(failed_methods)):

    if i % 2 == 0:
        print(failed_methods[i] + "(): ", end='')
    else:
        print(failed_methods[i])
